# -----------------
# >>> VARIABLES <<<
# -----------------
variable "k8s_cidr" {
  description = "CIDR for Kubernetes network"
  type        = string
}

variable "common_labels" {
  description = "Labels for all resources"
  default     = {}
  type        = map(string)
}

variable "ssh_key_files" {
  description = "List of SSH key files for enabling SSH access to servers"
  type        = list(string)
  default     = ["~/.ssh/id_ed25519.pub"]
}

variable "master_count" {
  description = "Number of master nodes"
  default     = 1
  type        = number
}

variable "worker_count" {
  description = "Number of worker nodes"
  default     = 1
  type        = number
}

variable "external_domain" {
  description = "Public domain for server FQDNs"
  type        = string
}

variable "ssh_allow_ip" {
  description = "IP address to whitelist for traffic to servers"
  type        = string
  default     = ""
}

variable "additional_resolv_conf_searchdomains" {
  description = "List of domains to add to resolv.conf"
  default     = []
  type        = list(string)
}

variable "hcloud_project_name" {
  description = "Project name (used for creating subdomain for DNS entries)"
  type        = string
}

# ---------------
# >>> OUTPUTS <<<
# ---------------
output "hostnames" {
  value = join(",", cloudflare_record.master.*.hostname)
}
