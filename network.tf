resource "hcloud_ssh_key" "this" {
  for_each = toset(var.ssh_key_files
  )
  name       = trimspace(reverse(split(" ", file(each.key)))[0])
  public_key = trimspace(file(each.key))
}

# ---------------
# >>> NETWORK <<<
# ---------------
resource "hcloud_network" "k8s" {
  name     = "k8s"
  ip_range = var.k8s_cidr
  labels   = local.common_labels
}

resource "hcloud_network_subnet" "control_plane" {
  network_id   = hcloud_network.k8s.id
  type         = "cloud"
  network_zone = "eu-central"
  ip_range     = cidrsubnet(var.k8s_cidr, 4, 0)
}

resource "hcloud_network_subnet" "data_plane" {
  network_id   = hcloud_network.k8s.id
  type         = "cloud"
  network_zone = "eu-central"
  ip_range     = cidrsubnet(var.k8s_cidr, 1, 1)
}

resource "hcloud_firewall" "control_plane" {
  name   = "control_plane"
  labels = local.common_labels

  dynamic "rule" {
    for_each = var.ssh_allow_ip == "" ? [] : local.firewall_rules
    content {
      description = rule.value.description
      direction   = "in"
      protocol    = rule.value.protocol
      port        = rule.value.port
      source_ips  = ["${var.ssh_allow_ip}/32"]
    }
  }

  apply_to {
    label_selector = "type==control_plane"
  }
}

resource "hcloud_firewall" "data_plane" {
  name   = "data_plane"
  labels = local.common_labels

  dynamic "rule" {
    for_each = var.ssh_allow_ip == "" ? [] : local.firewall_rules
    content {
      description = rule.value.description
      direction   = "in"
      protocol    = rule.value.protocol
      port        = rule.value.port
      source_ips  = ["${var.ssh_allow_ip}/32"]
    }
  }

  apply_to {
    label_selector = "type==data_plane"
  }
}

resource "hcloud_network_route" "default" {
  network_id  = hcloud_network.k8s.id
  gateway     = hcloud_server.master[0].network.*.ip[0]
  destination = "0.0.0.0/0"
}
