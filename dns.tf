data "cloudflare_zone" "this" {
  name = var.external_domain
}

resource "cloudflare_record" "master" {
  count = var.master_count

  zone_id = data.cloudflare_zone.this.zone_id
  name    = "master-${count.index + 1}.${var.hcloud_project_name}.hcloud"
  value   = hcloud_primary_ip.master[count.index].ip_address
  type    = "A"
  ttl     = 60
}
