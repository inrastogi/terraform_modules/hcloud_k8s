resource "hcloud_primary_ip" "master" {
  count = var.master_count

  name          = "master-${count.index + 1}"
  datacenter    = "fsn1-dc14"
  type          = "ipv4"
  auto_delete   = false
  assignee_type = "server"
  labels        = local.common_labels
}

resource "hcloud_server" "master" {
  count = var.master_count

  name        = "master-${count.index + 1}"
  server_type = "cpx11"
  image       = "debian-11"
  location    = "fsn1"
  ssh_keys    = [for ssh_key in hcloud_ssh_key.this : ssh_key.id]
  user_data = templatefile(
    "${path.module}/resources/cloud-init.yaml",
    {
      hostname                             = "master-${count.index + 1}"
      fqdn                                 = "master-${count.index + 1}.${var.hcloud_project_name}.hcloud.${var.external_domain}"
      ssh_keys                             = [for file in var.ssh_key_files : trimspace(file(file))]
      host_type                            = "router"
      default_gateway_ip                   = "dummy"
      resolv_conf_domain                   = "${var.hcloud_project_name}.hcloud.${var.external_domain}"
      additional_resolv_conf_searchdomains = var.additional_resolv_conf_searchdomains
    }
  )
  labels = merge(local.common_labels, {
    type = "control_plane"
  })

  network {
    network_id = hcloud_network.k8s.id
    ip         = cidrhost(hcloud_network_subnet.control_plane.ip_range, 2 + count.index)
  }
  public_net {
    ipv6_enabled = false
    ipv4         = hcloud_primary_ip.master[count.index].id
  }

  # **Note**: the depends_on is important when directly attaching the
  # server to a network. Otherwise Terraform will attempt to create
  # server and sub-network in parallel. This may result in the server
  # creation failing randomly.
  depends_on = [
    hcloud_network_subnet.control_plane,
  ]
}

resource "hcloud_server" "worker" {
  count = var.worker_count

  name        = "worker-${count.index + 1}"
  server_type = "cx21"
  image       = "debian-11"
  location    = "fsn1"
  ssh_keys    = [for ssh_key in hcloud_ssh_key.this : ssh_key.id]
  user_data = templatefile(
    "${path.module}/resources/cloud-init.yaml",
    {
      hostname                             = "worker-${count.index + 1}"
      fqdn                                 = "worker-${count.index + 1}.${var.hcloud_project_name}.hcloud.${var.external_domain}"
      ssh_keys                             = [for file in var.ssh_key_files : trimspace(file(file))]
      default_gateway_ip                   = cidrhost(var.k8s_cidr, 1)
      resolv_conf_domain                   = "${var.hcloud_project_name}.hcloud.${var.external_domain}"
      additional_resolv_conf_searchdomains = var.additional_resolv_conf_searchdomains
      host_type                            = "internal"
    }
  )
  labels = merge(local.common_labels, {
    type = "data_plane"
  })

  network {
    network_id = hcloud_network.k8s.id
    ip         = cidrhost(hcloud_network_subnet.data_plane.ip_range, 2 + count.index)
  }
  public_net {
    ipv4_enabled = false
    ipv6_enabled = false
  }

  # **Note**: the depends_on is important when directly attaching the
  # server to a network. Otherwise Terraform will attempt to create
  # server and sub-network in parallel. This may result in the server
  # creation failing randomly.
  depends_on = [
    hcloud_network_subnet.data_plane
  ]
}
