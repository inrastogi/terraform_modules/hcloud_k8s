locals {
  common_labels = merge(var.common_labels, {
    TerraformModule = "inrastogi.terraform_modules.hcloud_k8s"
  })
  firewall_rules = [
    { protocol = "icmp", port = null, description = "Allow ping" },
    { protocol = "tcp", port = 22, description = "Allow ssh" },
  ]
}
